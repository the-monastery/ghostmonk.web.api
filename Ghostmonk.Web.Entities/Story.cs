﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Ghostmonk.Web.Entities
{
    public class Story
    {
        [Key]
        public string Slug { get; set; }

        public string Title { get; set; }

        public DateTime PublishDate { get; set; }

        public bool IsPublished { get; set; }

        [DataType(DataType.MultilineText)]
        [AllowHtml] 
        public string Preamble { get; set; }

        [DataType(DataType.MultilineText)]
        [AllowHtml] 
        public string CompleteStory { get; set; }

        public virtual User Author { get; set; }

        public virtual Category Category { get; set; }

        public virtual List<string> Tags { get; set; } 
    }
}
