﻿using System.Data.Entity.Migrations;

namespace Ghostmonk.Web.Entities
{
    public class ModelMigrationConfiguration : DbMigrationsConfiguration<GhostmonkApiContext>
    {
        public ModelMigrationConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }
    }
}
