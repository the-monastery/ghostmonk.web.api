﻿using System.Collections.Generic;

namespace Ghostmonk.Web.Entities
{
    public class Category
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public List<Story> Stories { get; set; }

        public virtual List<string> Tags { get; set; } 
    }
}
