﻿using System.Data.Entity;

namespace Ghostmonk.Web.Entities
{
    public class GhostmonkApiContext : DbContext
    {
        public GhostmonkApiContext() : base("DefaultConnection")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        static GhostmonkApiContext()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<GhostmonkApiContext, ModelMigrationConfiguration>());
        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Story> Stories { get; set; }

        public DbSet<User> Users { get; set; }
    }
}
