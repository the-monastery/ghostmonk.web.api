﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Ghostmonk.Web.Entities
{
    public class GhostmonkApiRepository : IDisposable
    {
        public GhostmonkApiRepository()
        {
            Context = new GhostmonkApiContext();
        }

        public GhostmonkApiContext Context { get; private set; }

        public List<Category> GetAllCategories()
        {
            return Context.Categories.ToList();
        }

        public Category GetCategoryById(int id)
        {
            return Context.Categories.FirstOrDefault(c => c.Id == id);
        }

        public List<Story> GetAllStories()
        {
            return Context.Stories
                .OrderBy(s =>s.PublishDate)
                .Include(s => s.Author)
                .Include(s => s.Category).ToList();
        }

        public Story GetStoryBySlug(string slug)
        {
            return Context.Stories.FirstOrDefault(s => s.Slug == slug);
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
