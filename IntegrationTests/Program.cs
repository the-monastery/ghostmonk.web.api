﻿using System;
using Ghostmonk.Web.Entities;

namespace IntegrationTests
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var repository = new GhostmonkApiRepository())
            {
                repository.GetAllStories().ForEach(s => Console.WriteLine(s.Title));
            }
            Console.ReadLine();
        }
    }
}
