﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ghostmonk.Web.Api.Models;
using Ghostmonk.Web.Entities;

namespace Ghostmonk.Web.Api.Controllers
{
    public class StoryController : ApiController
    {
        private readonly ModelFactory factory = new ModelFactory();

        public HttpResponseMessage Get()
        {
            return ExecuteOnRepo(
                r => r.GetAllStories()
                    .Select(factory.Create<StorySummary>));
        }

        public HttpResponseMessage Get(string slug)
        {
            return ExecuteOnRepo(
                r => factory.Create<FullStory>(
                    r.GetStoryBySlug(slug)));
        }

        private HttpResponseMessage ExecuteOnRepo<T>(Func<GhostmonkApiRepository, T> action)
        {
            using (var repo = new GhostmonkApiRepository())
            {
                return Request.CreateResponse(
                    HttpStatusCode.OK,
                    action.Invoke(repo)
                );
            }
        }
    }
}
 