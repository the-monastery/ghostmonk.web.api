﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http.Formatting;
using System.Web.Http;
using Ghostmonk.Common.Web;
using Newtonsoft.Json.Serialization;

namespace Ghostmonk.Web.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                "Stories", 
                "stories/{slug}", 
                new { controller = "Story", slug = RouteParameter.Optional }
            );

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            jsonFormatter.MediaTypeMappings.Add(
                new RequestHeaderMapping(
                    HttpRequestHeader.Accept.ToString(), 
                    ContentType.Text.Html, 
                    StringComparison.InvariantCultureIgnoreCase, 
                    true, 
                    ContentType.Application.Json));
            
        }
    }
}
