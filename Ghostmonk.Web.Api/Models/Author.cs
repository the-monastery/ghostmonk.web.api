﻿namespace Ghostmonk.Web.Api.Models
{
    public class Author
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }
    }
}