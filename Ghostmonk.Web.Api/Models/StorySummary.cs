﻿namespace Ghostmonk.Web.Api.Models
{
    public class StorySummary : StoryDetails
    {
        public string Preamble { get; set; }
    }
}