﻿using Ghostmonk.Web.Entities;

namespace Ghostmonk.Web.Api.Models
{
    public class ModelFactory
    {
        public Author Create(User user)
        {
            return user == null ? null : new Author
            {
                UserName = user.UserName,
                FirstName = user.FirstName,
                LastName = user.LastName
            };
        }

        public T Create<T>(Story story) where T : StoryDetails
        {
            if (story == null)
            {
                return null;
            }

            StoryDetails output = typeof(T) == typeof(FullStory)
                ? (StoryDetails)new FullStory{CompleteStory = story.CompleteStory}
                : new StorySummary{Preamble = story.Preamble};

            output.Author = Create(story.Author);
            output.Category = story.Category == null ? null : story.Category.Title;
            output.Slug = story.Slug;
            output.PublishDate = story.PublishDate;
            output.Title = story.Title;

            return (T)output;
        }
    }
}