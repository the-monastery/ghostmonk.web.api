﻿namespace Ghostmonk.Web.Api.Models
{
    public class FullStory : StoryDetails
    {
        public string CompleteStory { get; set; }
    }
}