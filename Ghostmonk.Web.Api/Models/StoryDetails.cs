﻿using System;

namespace Ghostmonk.Web.Api.Models
{
    public class StoryDetails
    {
        public string Title { get; set; }

        public string Slug { get; set; }

        public DateTime PublishDate { get; set; }

        public Author Author { get; set; }

        public string Category { get; set; }
    }
}