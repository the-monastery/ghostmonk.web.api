﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Web;
using System.Web.Http;
using Ghostmonk.Web.Entities;

namespace Ghostmonk.Web.Api
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            Database.SetInitializer<GhostmonkApiContext>(null);

            try
            {
                using (var context = new GhostmonkApiContext())
                {
                    if (context.Database.Exists())
                    {
                        return;
                    }
                    ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();
                }
            }
            catch (Exception ex)
            {
                throw;
                //throw new InvalidOperationException("Database could not be initialized. EX:" + ex.Message);
            }
        }
    }
}
