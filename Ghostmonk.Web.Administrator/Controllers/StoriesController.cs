﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Ghostmonk.Web.Administrator.Models;
using Ghostmonk.Web.Entities;

namespace Ghostmonk.Web.Administrator.Controllers
{
    public class StoriesController : Controller
    {
        private GhostmonkApiContext db = new GhostmonkApiContext();

        // GET: Stories
        public async Task<ActionResult> Index()
        {
            return View(await db.Stories.ToListAsync());
        }

        // GET: Stories/Details/5
        public async Task<ActionResult> Details(string slug)
        {
            if (string.IsNullOrEmpty(slug))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Story story = await db.Stories.FindAsync(slug);
            if (story == null)
            {
                return HttpNotFound();
            }
            return View(story);
        }

        // GET: Stories/Create
        public ActionResult Create()
        {
            CreateStoryViewModel viewModel = new CreateStoryViewModel
            {
                Categories = db.Categories.ToList(),
                Authors = db.Users.ToList()
            };
            return View(viewModel);
        }

        // POST: Stories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateStoryViewModel storyVm)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            //TODO: All posts are by me
            storyVm.Story.Author = db.Users.FirstOrDefault(a => a.UserName == "ghostmonk");
            storyVm.Story.Category = db.Categories.FirstOrDefault(c => c.Id == storyVm.Category);
            db.Stories.Add(storyVm.Story);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // GET: Stories/Edit/5
        public async Task<ActionResult> Edit(string slug)
        {
            if (string.IsNullOrEmpty(slug))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Story story = await db.Stories.FindAsync(slug);
            if (story == null)
            {
                return HttpNotFound();
            }
            return View(story);
        }

        // POST: Stories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Title,PublishDate,IsPublished,Slug,Preamble,CompleteStory")] Story story)
        {
            if (ModelState.IsValid)
            {
                db.Entry(story).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(story);
        }

        // GET: Stories/Delete/5
        public async Task<ActionResult> Delete(string slug)
        {
            if (string.IsNullOrEmpty(slug))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Story story = await db.Stories.FindAsync(slug);
            if (story == null)
            {
                return HttpNotFound();
            }
            return View(story);
        }

        // POST: Stories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string slug)
        {
            Story story = await db.Stories.FindAsync(slug);
            db.Stories.Remove(story);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
