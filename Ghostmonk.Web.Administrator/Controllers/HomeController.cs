﻿using System.Web.Mvc;

namespace Ghostmonk.Web.Administrator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
