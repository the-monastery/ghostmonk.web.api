﻿using System.Collections.Generic;
using Ghostmonk.Web.Entities;

namespace Ghostmonk.Web.Administrator.Models
{
    public class CreateStoryViewModel
    {
        public Story Story { get; set; }

        public int Category { get; set; }

        public IEnumerable<User> Authors { get; set; }

        public IEnumerable<Category> Categories { get; set; }
    }
}